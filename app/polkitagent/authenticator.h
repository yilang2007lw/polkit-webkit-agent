/*
 * Copyright (C) 2009 Red Hat, Inc.
 *               2013 Deepin, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: David Zeuthen <davidz@redhat.com>
 *         Long Wei <yilang2007lw@gmail.com>
 */

#ifndef __AUTHENTICATOR_H
#define __AUTHENTICATOR_H

#include <glib-object.h>
#include "jsextension.h"

G_BEGIN_DECLS

#define POLKIT_WEBKIT_TYPE_AUTHENTICATOR          (polkit_webkit_authenticator_get_type())
#define POLKIT_WEBKIT_AUTHENTICATOR(o)            (G_TYPE_CHECK_INSTANCE_CAST ((o), POLKIT_WEBKIT_TYPE_AUTHENTICATOR, PolkitWebkitAuthenticator))
#define POLKIT_WEBKIT_AUTHENTICATOR_CLASS(k)      (G_TYPE_CHECK_CLASS_CAST((k), POLKIT_WEBKIT_TYPE_AUTHENTICATOR, PolkitWebkitAuthenticatorClass))
#define POLKIT_WEBKIT_AUTHENTICATOR_GET_CLASS(o)  (G_TYPE_INSTANCE_GET_CLASS ((o), POLKIT_WEBKIT_TYPE_AUTHENTICATOR, PolkitWebkitAuthenticatorClass))
#define POLKIT_WEBKIT_IS_AUTHENTICATOR(o)         (G_TYPE_CHECK_INSTANCE_TYPE ((o), POLKIT_WEBKIT_TYPE_AUTHENTICATOR))
#define POLKIT_WEBKIT_IS_AUTHENTICATOR_CLASS(k)   (G_TYPE_CHECK_CLASS_TYPE ((k), POLKIT_WEBKIT_TYPE_AUTHENTICATOR))

typedef struct _PolkitWebkitAuthenticator PolkitWebkitAuthenticator;
typedef struct _PolkitWebkitAuthenticatorClass PolkitWebkitAuthenticatorClass;

GType                      polkit_webkit_authenticator_get_type   (void) G_GNUC_CONST;
PolkitWebkitAuthenticator  *polkit_webkit_authenticator_new       (const gchar              *action_id,
                                                                  const gchar              *message,
                                                                  const gchar              *icon_name,
                                                                  PolkitDetails            *details,
                                                                  const gchar              *cookie,
                                                                  GList                    *identities);
void                       polkit_webkit_authenticator_initiate   (PolkitWebkitAuthenticator *authenticator);
void                       polkit_webkit_authenticator_cancel     (PolkitWebkitAuthenticator *authenticator);

JS_EXPORT_API   gchar       *polkitagent_get_action_id (JSData *data);
JS_EXPORT_API   gchar       *polkitagent_get_action_message (JSData *data);
JS_EXPORT_API   gchar       *polkitagent_get_icon_name (JSData *data);
JS_EXPORT_API   gchar       *polkitagent_get_action_description (JSData *data);
JS_EXPORT_API   gchar       *polkitagent_get_vendor_name (JSData *data);
JS_EXPORT_API   gchar       *polkitagent_get_vendor_url (JSData *data);
JS_EXPORT_API   JSObjectRef  polkitagent_get_users (JSData *data);
JS_EXPORT_API   void         polkitagent_confirm_authentication (const gchar *username, const gchar *password, JSData *data);
JS_EXPORT_API   void         polkitagent_cancel_authentication (JSData *data);
JS_EXPORT_API   void         polkitagent_draw_dialog (JSValueRef canvas);

G_END_DECLS

#endif 
