/*
 * Copyright (C) 2009 Red Hat, Inc.
 *               2013 Deepin, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: David Zeuthen <davidz@redhat.com>
 *         Long Wei <yilang2007lw@gmail.com>
 */

#include <string.h>
#include <glib/gi18n.h>

#include "listener.h"
#include "authenticator.h"

struct _PolkitWebkitListener {
    PolkitAgentListener parent_instance;

    PolkitWebkitAuthenticator *active_authenticator;
};

struct _PolkitWebkitListenerClass {
    PolkitAgentListenerClass parent_class;
};

static void polkit_webkit_listener_initiate_authentication (PolkitAgentListener  *listener,
                                                            const gchar          *action_id,
                                                            const gchar          *message,
                                                            const gchar          *icon_name,
                                                            PolkitDetails        *details,
                                                            const gchar          *cookie,
                                                            GList                *identities,
                                                            GCancellable         *cancellable,
                                                            GAsyncReadyCallback   callback,
                                                            gpointer              user_data);

static gboolean polkit_webkit_listener_initiate_authentication_finish (PolkitAgentListener  *listener,
                                                                       GAsyncResult         *res,
                                                                       GError              **error);

G_DEFINE_TYPE (PolkitWebkitListener, polkit_webkit_listener, POLKIT_AGENT_TYPE_LISTENER);

static void
polkit_webkit_listener_init (PolkitWebkitListener *listener)
{

}

static void
polkit_webkit_listener_finalize (GObject *object)
{
    g_debug ("listener finalize");

    PolkitWebkitListener *listener = POLKIT_WEBKIT_LISTENER (object);

    if (listener->active_authenticator != NULL) {
        g_object_unref (listener->active_authenticator);
    }

    if (G_OBJECT_CLASS (polkit_webkit_listener_parent_class)->finalize != NULL)
        G_OBJECT_CLASS (polkit_webkit_listener_parent_class)->finalize (object);
}

static void
polkit_webkit_listener_class_init (PolkitWebkitListenerClass *klass)
{
    g_debug ("listener class init");

    GObjectClass *gobject_class;
    PolkitAgentListenerClass *listener_class;

    gobject_class = G_OBJECT_CLASS (klass);
    listener_class = POLKIT_AGENT_LISTENER_CLASS (klass);

    gobject_class->finalize = polkit_webkit_listener_finalize;

    listener_class->initiate_authentication          = polkit_webkit_listener_initiate_authentication;
    listener_class->initiate_authentication_finish   = polkit_webkit_listener_initiate_authentication_finish;
}

PolkitAgentListener *
polkit_webkit_listener_new (void)
{
    g_debug ("listener new");

    return POLKIT_AGENT_LISTENER (g_object_new (POLKIT_WEBKIT_TYPE_LISTENER, NULL));
}

typedef struct {
    PolkitWebkitListener *listener;
    PolkitWebkitAuthenticator *authenticator;

    GSimpleAsyncResult *simple;
    GCancellable *cancellable;

    gulong cancel_id;
} AuthData;

static AuthData *
auth_data_new (PolkitWebkitListener *listener,
               PolkitWebkitAuthenticator *authenticator,
               GSimpleAsyncResult *simple,
               GCancellable *cancellable)
{
    g_debug ("listener:auth data new");

    AuthData *data;

    data = g_new0 (AuthData, 1);
    data->listener = g_object_ref (listener);
    data->authenticator = g_object_ref (authenticator);
    data->simple = g_object_ref (simple);
    data->cancellable = g_object_ref (cancellable);
    return data;
}

static void
auth_data_free (AuthData *data)
{
    g_debug ("listener:auth data free");

    if (data->listener != NULL) {
        g_object_unref (data->listener);
    }

    if (data->authenticator != NULL) {
        g_object_unref (data->authenticator);
    }

    if (data->simple != NULL) { 
        g_object_unref (data->simple);
    }

    if (data->cancellable != NULL && data->cancel_id > 0)
        g_signal_handler_disconnect (data->cancellable, data->cancel_id);

    g_object_unref (data->cancellable);
    g_free (data);
}

static void
authenticator_completed (PolkitWebkitAuthenticator *authenticator,
                         gboolean                  gained_authorization,
                         gboolean                  dismissed,
                         gpointer                  user_data)
{
    g_debug ("listener:authenticator completed");

    AuthData *data = user_data;

    g_object_unref (authenticator);

    if (dismissed) {
        g_simple_async_result_set_error (data->simple,
                                         POLKIT_ERROR,
                                         POLKIT_ERROR_CANCELLED,
                                         _("Authentication dialog was dismissed by the user"));
    }

    g_simple_async_result_complete (data->simple);
    g_object_unref (data->simple);

    auth_data_free (data);
}

static void
cancelled_cb (GCancellable *cancellable,
              gpointer user_data)
{
    g_debug ("listener:cancelled cb");

    AuthData *data = user_data;

    polkit_webkit_authenticator_cancel (data->authenticator);
}

static void
polkit_webkit_listener_initiate_authentication (PolkitAgentListener  *agent_listener,
                                               const gchar          *action_id,
                                               const gchar          *message,
                                               const gchar          *icon_name,
                                               PolkitDetails        *details,
                                               const gchar          *cookie,
                                               GList                *identities,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data)
{
    g_debug ("listener:initiate authentication");

    PolkitWebkitListener *listener = POLKIT_WEBKIT_LISTENER (agent_listener);
    GSimpleAsyncResult *simple;
    PolkitWebkitAuthenticator *authenticator;
    AuthData *data;

    simple = g_simple_async_result_new (G_OBJECT (listener),
                                        callback,
                                        user_data,
                                        polkit_webkit_listener_initiate_authentication);

    authenticator = polkit_webkit_authenticator_new (action_id,
                                                     message,
                                                     icon_name,
                                                     details,
                                                     cookie,
                                                     identities);
    if (authenticator == NULL) {
        g_simple_async_result_set_error (simple,
                                         POLKIT_ERROR,
                                         POLKIT_ERROR_FAILED,
                                         "Error creating authentication object");
        g_simple_async_result_complete (simple);
        return ;
    }

    data = auth_data_new (listener, authenticator, simple, cancellable);

    g_signal_connect (authenticator,
                      "completed",
                      G_CALLBACK (authenticator_completed),
                      data);

    if (cancellable != NULL) {
        data->cancel_id = g_signal_connect (cancellable,
                                            "cancelled",
                                            G_CALLBACK (cancelled_cb),
                                            data);
    }

    listener->active_authenticator = authenticator;

    polkit_webkit_authenticator_initiate (POLKIT_WEBKIT_AUTHENTICATOR (listener->active_authenticator));
}

static gboolean
polkit_webkit_listener_initiate_authentication_finish (PolkitAgentListener  *listener,
                                                      GAsyncResult         *res,
                                                      GError              **error)
{
    g_debug ("listener:initiate authentication finish");

    GSimpleAsyncResult *simple = G_SIMPLE_ASYNC_RESULT (res);

    g_warn_if_fail (g_simple_async_result_get_source_tag (simple) == polkit_webkit_listener_initiate_authentication);

    if (g_simple_async_result_propagate_error (simple, error)) {
        g_debug ("listener:authentication finish error");

        return FALSE;
    }
    g_debug ("listener:initiate authentication finish succeed");

    return TRUE;
}
