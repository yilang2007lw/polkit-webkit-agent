/*
 * Copyright (C) 2009 Red Hat, Inc.
 *               2013 Deepin, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: David Zeuthen <davidz@redhat.com>
 *         Long Wei <yilang2007lw@gmail.com>
 */

#include <string.h>
#include <sys/types.h>
#include <pwd.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <polkit/polkit.h>
#include <polkitagent/polkitagent.h>
#include "dwebview.h"
#include "utils.h"
#include "authenticator.h"
#include "polkitwebkitmarshal.h"

#define POLKITAGENT_HTML_PATH "file://"RESOURCE_DIR"/polkitagent/index.html"

struct _PolkitWebkitAuthenticator {
    GObject parent_instance;

    PolkitAuthority *authority;
    gchar *action_id;
    gchar *message;
    gchar *icon_name;
    PolkitDetails *details;
    gchar *cookie;
    GList *identities;
    PolkitActionDescription *action_desc;
    gchar **users;
    gboolean gained_authorization;
    gboolean was_cancelled;
    gint num_tries;
    PolkitAgentSession *session;
    GtkWidget *dialog;
    GtkWidget *webview;
    GMainLoop *loop;
};

struct _PolkitWebkitAuthenticatorClass {
    GObjectClass parent_class;

};

enum {
    COMPLETED_SIGNAL,
    LAST_SIGNAL,
};

static guint signals[LAST_SIGNAL] = {0};

G_DEFINE_TYPE (PolkitWebkitAuthenticator, polkit_webkit_authenticator, G_TYPE_OBJECT);

static void
polkit_webkit_authenticator_init (PolkitWebkitAuthenticator *authenticator)
{
    g_debug ("polkit webkit authenticator init");
}

static void
polkit_webkit_authenticator_finalize (GObject *object)
{
    g_debug ("polkit webkit authenticator finalize");

    PolkitWebkitAuthenticator *authenticator;

    authenticator = POLKIT_WEBKIT_AUTHENTICATOR (object);

    if (authenticator->authority != NULL)
        g_object_unref (authenticator->authority);

    g_free (authenticator->action_id);
    g_free (authenticator->message);
    g_free (authenticator->icon_name);

    if (authenticator->details != NULL)
        g_object_unref (authenticator->details);

    g_free (authenticator->cookie);
    g_list_foreach (authenticator->identities, (GFunc) g_object_unref, NULL);
    g_list_free (authenticator->identities);

    if (authenticator->action_desc != NULL)
        g_object_unref (authenticator->action_desc);
    g_strfreev (authenticator->users);

    if (authenticator->session != NULL)
        g_object_unref (authenticator->session);

    if (authenticator->dialog != NULL)
        gtk_widget_destroy (authenticator->dialog);

    if (authenticator->webview != NULL)
        gtk_widget_destroy (authenticator->webview);

    if (authenticator->loop != NULL)
        g_main_loop_unref (authenticator->loop);

    if (G_OBJECT_CLASS (polkit_webkit_authenticator_parent_class)->finalize != NULL)
        G_OBJECT_CLASS (polkit_webkit_authenticator_parent_class)->finalize (object);
}

static void
polkit_webkit_authenticator_class_init (PolkitWebkitAuthenticatorClass *klass)
{
    g_debug ("polkit webkit authenticator class init");

    GObjectClass *gobject_class;

    gobject_class = G_OBJECT_CLASS (klass);

    gobject_class->finalize = polkit_webkit_authenticator_finalize;

    signals[COMPLETED_SIGNAL] = g_signal_new ("completed",
                                              POLKIT_WEBKIT_TYPE_AUTHENTICATOR,
                                              G_SIGNAL_RUN_LAST,
                                              0,                      /* class offset     */
                                              NULL,                   /* accumulator      */
                                              NULL,                   /* accumulator data */
                                              _polkit_webkit_marshal_VOID__BOOLEAN_BOOLEAN,
                                              G_TYPE_NONE,
                                              2,
                                              G_TYPE_BOOLEAN,
                                              G_TYPE_BOOLEAN);
}

static PolkitActionDescription *
get_desc_for_action (PolkitAuthority *authority, const gchar *action_id)
{
    GList *action_descs;
    GList *l;
    PolkitActionDescription *result = NULL;

    action_descs = polkit_authority_enumerate_actions_sync (authority, NULL, NULL);
    for (l = action_descs; l != NULL; l = l->next) {
        PolkitActionDescription *action_desc = POLKIT_ACTION_DESCRIPTION (l->data);

        if (strcmp (polkit_action_description_get_action_id (action_desc), action_id) == 0) {
            result = g_object_ref (action_desc);
            goto out;
        }
    }
out:
    g_list_foreach (action_descs, (GFunc) g_object_unref, NULL);
    g_list_free (action_descs);

    return result;
}

static void
on_dialog_deleted (GtkWidget *widget, GdkEvent  *event, gpointer user_data)
{
    g_debug ("on dialog deleted");

    PolkitWebkitAuthenticator *authenticator = POLKIT_WEBKIT_AUTHENTICATOR (user_data);

    polkit_webkit_authenticator_cancel (authenticator);
}

static gboolean
move_window (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{
    g_debug ("move window");

    PolkitWebkitAuthenticator *authenticator = POLKIT_WEBKIT_AUTHENTICATOR (user_data);

    if ((event->x > 420 && event->y < 30) || (event->x > 325 && event->y > 270)) {
        g_debug ("move window:html click area");
        return TRUE;
    }

    if (event->button == 1) {
        g_debug ("move window:in drag x_root->%g, y_root->%g", event->x_root, event->y_root);

        gtk_widget_set_can_focus (widget, TRUE);
        gtk_widget_grab_focus (widget);

        gtk_window_begin_move_drag (GTK_WINDOW (authenticator->dialog), 
                                    event->button, 
                                    event->x_root,
                                    event->y_root,
                                    event->time);

    }

    return FALSE;
}

PolkitWebkitAuthenticator *
polkit_webkit_authenticator_new (const gchar     *action_id,
                                 const gchar     *message,
                                 const gchar     *icon_name,
                                 PolkitDetails   *details,
                                 const gchar     *cookie,
                                 GList           *identities)
{
    g_debug ("polkit webkit authenticator new");

    PolkitWebkitAuthenticator *authenticator;
    GList *l;
    guint n;
    GError *error;

    authenticator = POLKIT_WEBKIT_AUTHENTICATOR (g_object_new (POLKIT_WEBKIT_TYPE_AUTHENTICATOR, NULL));

    error = NULL;
    authenticator->authority = polkit_authority_get_sync (NULL /* GCancellable* */, &error);
    if (authenticator->authority == NULL) {
        g_critical ("Error getting authority: %s", error->message);
        g_error_free (error);
        goto error;
    }

    authenticator->action_id = g_strdup (action_id);
    authenticator->message = g_strdup (message);
    authenticator->icon_name = g_strdup (icon_name);
    if (details != NULL)
        authenticator->details = g_object_ref (details);
    authenticator->cookie = g_strdup (cookie);
    authenticator->identities = g_list_copy (identities);
    g_list_foreach (authenticator->identities, (GFunc) g_object_ref, NULL);

    authenticator->action_desc = get_desc_for_action (authenticator->authority,
                                                      authenticator->action_id);
    if (authenticator->action_desc == NULL)
        goto error;

    authenticator->users = g_new0 (gchar *, g_list_length (authenticator->identities) + 1);
    for (l = authenticator->identities, n = 0; l != NULL; l = l->next, n++) {
        PolkitUnixUser *user = POLKIT_UNIX_USER (l->data);
        uid_t uid;
        struct passwd *passwd;

        uid = polkit_unix_user_get_uid (user);
        passwd = getpwuid (uid);
        authenticator->users[n] = g_strdup (passwd->pw_name);
    }

    authenticator->was_cancelled = FALSE;
    authenticator->num_tries = 0;

    authenticator->dialog = create_web_container (FALSE, TRUE);
    gtk_window_set_decorated (GTK_WINDOW (authenticator->dialog), FALSE);

    authenticator->webview = d_webview_new_with_uri (POLKITAGENT_HTML_PATH);
    g_object_set_data (G_OBJECT(authenticator->webview), "authenticator", authenticator);

    g_signal_connect (authenticator->dialog, "delete-event", G_CALLBACK (on_dialog_deleted), authenticator);
    g_signal_connect (authenticator->dialog, "button-press-event", G_CALLBACK (move_window), authenticator);
    g_signal_connect (authenticator->webview, "draw", G_CALLBACK (erase_background), NULL);

    gtk_container_add (GTK_CONTAINER (authenticator->dialog), GTK_WIDGET (authenticator->webview));

    //gtk_window_set_position (GTK_WINDOW (authenticator->dialog), GTK_WIN_POS_CENTER);
    gtk_widget_realize (authenticator->dialog);
    gtk_widget_realize (authenticator->webview);

    gtk_widget_show_all (authenticator->dialog);

    GdkScreen *screen = gdk_screen_get_default ();
    gint screen_width = gdk_screen_get_width (screen);
    gint screen_height = gdk_screen_get_height (screen);
    gint width = gdk_window_get_width (gtk_widget_get_window (authenticator->dialog));
    gint height = gdk_window_get_height (gtk_widget_get_window (authenticator->dialog));

    gtk_window_move (GTK_WINDOW (authenticator->dialog), (screen_width) / 2 - width , (screen_height) / 2 - height);

    return authenticator;

error:
    g_object_unref (authenticator);
    return NULL;
}

static PolkitWebkitAuthenticator *
get_authenticator_from_dialog (JSData *data)
{
    return POLKIT_WEBKIT_AUTHENTICATOR (g_object_get_data (data->webview, "authenticator"));
}

JS_EXPORT_API   
gchar *polkitagent_get_action_id (JSData *data)
{
    g_debug ("polkitagent get action id");

    gchar *action_id = NULL;

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get action id:get authenticator failed\n");
        return action_id;
    }
    action_id = g_strdup (authenticator->action_id);
    g_object_unref (authenticator);

    return action_id;
}

JS_EXPORT_API   
gchar *polkitagent_get_action_message (JSData *data)
{
    g_debug ("polkitagent get action message");

    gchar *message = NULL;

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get action message:get authenticator failed\n");
        return message;
    }
    message = g_strdup (authenticator->message);
    g_object_unref (authenticator);

    return message;
}

JS_EXPORT_API   
gchar *polkitagent_get_icon_name (JSData *data)
{
    g_debug ("polkitagent get icon name");

    gchar *icon_name = NULL;

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get icon name:get authenticator failed\n");
        return icon_name;
    }
    icon_name = g_strdup (authenticator->icon_name);
    g_object_unref (authenticator);

    return icon_name;
}

JS_EXPORT_API   
gchar *polkitagent_get_action_description (JSData *data)
{
    g_debug ("polkitagent get action description");

    gchar *description = NULL;

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get action description:get authenticator failed\n");
        return description;
    }

    PolkitActionDescription *action_desc = g_object_ref (authenticator->action_desc);
    if (action_desc != NULL) {
        description = g_strdup (polkit_action_description_get_description (action_desc));
    }

    g_object_unref (action_desc);
    g_object_unref (authenticator);

    return description;
}

JS_EXPORT_API   
gchar *polkitagent_get_vendor_name (JSData *data)
{
    g_debug ("polkitagent get vendor name");

    gchar *vendor_name = NULL;

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get vendor name:get authenticator failed\n");
        return vendor_name;
    }

    PolkitActionDescription *action_desc = g_object_ref (authenticator->action_desc);
    if (action_desc != NULL) {
        vendor_name = g_strdup (polkit_action_description_get_vendor_name (action_desc));
    }

    g_object_unref (action_desc);
    g_object_unref (authenticator);

    return vendor_name;
}

JS_EXPORT_API   
gchar *polkitagent_get_vendor_url (JSData *data)
{
    g_debug ("polkitagent get vendor url");

    gchar *vendor_url = NULL;

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get vendor url:get authenticator failed\n");
        return vendor_url;
    }

    PolkitActionDescription *action_desc = g_object_ref (authenticator->action_desc);
    if (action_desc != NULL) {
        vendor_url = g_strdup (polkit_action_description_get_vendor_url (action_desc));
    }

    g_object_unref (action_desc);
    g_object_unref (authenticator);

    return vendor_url;
}

JS_EXPORT_API
JSObjectRef  polkitagent_get_users (JSData *data) 
{
    g_debug ("polkitagent get users");

    JSObjectRef array = json_array_create ();

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("get users:get authenticator failed\n");
        return array;
    }

    int i;
    for (i = 0; i < g_list_length (authenticator->identities); i++) {
        json_array_insert (array, i, jsvalue_from_cstr (get_global_context (), authenticator->users[i]));
    }

    g_object_unref (authenticator);

    return array;
}

static void
session_request (PolkitAgentSession *session, const char *request, gboolean echo_on, gpointer user_data)
{
    g_debug ("session request");

    gchar *password = g_strdup (user_data);
    if (password == NULL) {
        g_warning ("session request:password is NULL\n");
        polkit_agent_session_cancel (session);

    } else {
        polkit_agent_session_response (session, password);
        g_free (password);
    }
}

static void
session_show_error (PolkitAgentSession *session, const gchar *msg, gpointer user_data)
{
    g_debug ("session show error");
    //PolkitWebkitAuthenticator *authenticator = POLKIT_WEBKIT_AUTHENTICATOR (user_data);
}

static void
session_show_info (PolkitAgentSession *session, const gchar *msg, gpointer user_data)
{
    g_debug ("session show info"); 
    //PolkitWebkitAuthenticator *authenticator = POLKIT_WEBKIT_AUTHENTICATOR (user_data);
}

static void
session_completed (PolkitAgentSession *session, gboolean gained_authorization, gpointer user_data)
{
    g_debug ("session completed:gained authorization:%d\n", gained_authorization);

    PolkitWebkitAuthenticator *authenticator = POLKIT_WEBKIT_AUTHENTICATOR (user_data);
    authenticator->gained_authorization = gained_authorization;

    if (gained_authorization || authenticator->num_tries == 2) {
        g_signal_emit_by_name (authenticator, 
                               "completed", 
                               authenticator->gained_authorization, 
                               authenticator->was_cancelled);

    } else {
        authenticator->num_tries = authenticator->num_tries + 1;
        js_post_message_simply("auth", "{\"times\":\"%d\"}", authenticator->num_tries);
    }

    if (authenticator->loop != NULL) {
        g_main_loop_quit (authenticator->loop);
    }
}

void
polkit_webkit_authenticator_initiate (PolkitWebkitAuthenticator *authenticator)
{
    g_debug ("polkit webkit authenticator initiate"); 
}

void
polkit_webkit_authenticator_start (PolkitWebkitAuthenticator *authenticator, 
                                   const gchar *username, 
                                   const gchar *password)
{
    g_debug ("authenticator start: username %s, password %s", username, password);

    PolkitIdentity *identity = NULL;

    authenticator->loop = g_main_loop_new (NULL, FALSE);
    identity = polkit_unix_user_new_for_name (username, NULL);
    authenticator->session = polkit_agent_session_new (identity, authenticator->cookie);

    g_object_unref (identity);

    g_signal_connect (authenticator->session,
                      "request",
                      G_CALLBACK (session_request),
                      (void *)password);

    g_signal_connect (authenticator->session,
                      "show-info",
                      G_CALLBACK (session_show_info),
                      authenticator);

    g_signal_connect (authenticator->session,
                      "show-error",
                      G_CALLBACK (session_show_error),
                      authenticator);

    g_signal_connect (authenticator->session,
                      "completed",
                      G_CALLBACK (session_completed),
                      authenticator);

    polkit_agent_session_initiate (authenticator->session);

    g_main_loop_run (authenticator->loop);
}

void
polkit_webkit_authenticator_cancel (PolkitWebkitAuthenticator *authenticator)
{
    g_debug ("polkit webkit authenticator cancel");

    authenticator->was_cancelled = TRUE;
    if (authenticator->session != NULL) {
        polkit_agent_session_cancel (authenticator->session);
    }

    g_signal_emit_by_name (authenticator,
                           "completed",
                           authenticator->gained_authorization,
                           authenticator->was_cancelled);

    if (authenticator->loop != NULL) {
        g_main_loop_quit (authenticator->loop);
    }
}

JS_EXPORT_API   
void polkitagent_confirm_authentication (const gchar *username, const gchar *password, JSData *data)
{
    g_debug ("polkitagent confirm authentication");

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("polkitagent confirm authentication:get authenticator failed\n");
        return;
    }
    polkit_webkit_authenticator_start (authenticator, username, password);
    g_object_unref (authenticator);
}

JS_EXPORT_API 
void polkitagent_cancel_authentication (JSData *data)
{
    g_debug ("polkitagent cancel authentication");

    PolkitWebkitAuthenticator *authenticator = g_object_ref (get_authenticator_from_dialog (data));
    if (authenticator == NULL) {
        g_warning ("polkitagent cancel authentication:get authenticator failed\n");
        return;
    }
    polkit_webkit_authenticator_cancel (authenticator);
    g_object_unref (authenticator);
}

JS_EXPORT_API   
void polkitagent_draw_dialog (JSValueRef canvas)
{
    cairo_t* cr =  fetch_cairo_from_html_canvas (get_global_context (), canvas);
    cairo_set_source_rgba (cr, 0, 0, 0, 0.85);
    cairo_paint (cr);
    canvas_custom_draw_did (cr, NULL);
}

JS_EXPORT_API
gchar* polkitagent_get_login_user ()
{
    return (gchar *)g_get_user_name ();
}
