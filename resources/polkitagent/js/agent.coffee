#Copyright (c) 2013 Deepin, Inc.
#              2013 yilang 
#
#Author:      Long Wei <yilang2007lw@gmail.com>
#Maintainer:  Long Wei <yilang2007lw@gmail.com>
#
#This program is free software; you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation; either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program; if not, see <http://www.gnu.org/licenses/>.

DIALOG_WIDTH  = 450.0
DIALOG_HEIGHT = 300.0

dialog = $("#dialog")
dialog.width = DIALOG_WIDTH
dialog.height = DIALOG_HEIGHT
DCore.PolkitAgent.draw_dialog(dialog)

unique_array = (array) ->
    result = []
    for item in array
        if item not in result
            result.push(item)
    return result

class Agent extends Widget
    constructor: (@id) ->
        super
        @in_auth = false

        @title = create_element("div", "Title", @element)

        @text = create_element("span", "Text", @title)
        @text.innerText = _("Authentication")
        @close = create_element("span", "Close", @title)
        @close.addEventListener("click", (e) =>
            DCore.PolkitAgent.cancel_authentication()
        )

        @content = create_element("div", "Content", @element)

        @icon = create_element("div", "Icon", @content)
        icon_name = DCore.PolkitAgent.get_icon_name()
        icon_path = null
        if icon_name?
            try
                icon_path = DCore.get_theme_icon(icon_name,48)
            catch error
                echo "get theme icon failed"
        if not icon_path?
            icon_path = "images/icon.png"
        @icon_img = create_img("IconImg", icon_path, @icon)

        @message = create_element("p", "Message", @content)
        @message.innerText = DCore.PolkitAgent.get_action_message()

        @description = create_element("p", "Description", @content)
        @description.innerText = DCore.PolkitAgent.get_action_description()

        @blank = create_element("div", "Blank", @content)

        @users = create_element("p", "Users", @content)
        @users.innerHTML = "<span>" + _("Username:") + "</span>"
        @user_select = create_element("select", "", @users)
        login_user = DCore.PolkitAgent.get_login_user()
        for user in unique_array(DCore.PolkitAgent.get_users())
            opt = create_element("option","", @user_select)
            opt.setAttribute("value", user)
            opt.innerText = user
            if user == login_user
                opt.setAttribute("selected", "selected")

        @password = create_element("p", "Pwd", @content)
        @password.innerHTML = "<span>" + _("Password:") + "</span>"
        @pwd_input = create_element("input", "", @password)
        @pwd_input.setAttribute("type", "password")
        @pwd_input.setAttribute("autofocus", true)

        @action = create_element("p", "action", @content)
        @action.innerHTML = "<span>" + _("Action ID:") + "</span>"
        @action.innerHTML += "<i>" + DCore.PolkitAgent.get_action_id() + "</i>"

        #@vendor_name = create_element("p", "VendorName", @content)
        #@vendor_name.innerHTML = "<span>Vendor Name:</span>"
        #@vendor_name.innerHTML += "<i>" + DCore.PolkitAgent.get_vendor_name() + "</i>"
        @info = create_element("p", "Info", @content)

        @line = create_element("div", "Line", @element)

        @footer = create_element("div", "Footer", @element)

        @ok = create_element("div", "Ok", @footer)
        @ok.innerText = _("OK")
        @ok.addEventListener("click", (e) =>
            @start_auth()
        )

        @cancel = create_element("div", "Cancel", @footer)
        @cancel.innerText = _("Cancel")
        @cancel.addEventListener("click", (e) =>
            @in_auth = false
            DCore.PolkitAgent.cancel_authentication()
        )

    start_auth: ->
        @info.innerHTML = ""
        if @pwd_input.value?
            if not @in_auth
                username = @user_select.options[@user_select.selectedIndex].value
                password = @pwd_input.value
                @in_auth = true
                DCore.PolkitAgent.confirm_authentication(username, password)
        else
            @pwd_input.focus()

    auth_failed: (time) ->
        @pwd_input.value = ""
        #@info.innerHTML = "<span>" + _("Authentication failed:") + "</span><i>" + _("You have tried") + time + _("/3 times.") + "</i>"
        @info.innerText =  _("Authentication failed:") + _("You have tried") + time + _("/3 times.")
        @in_auth = false
        @pwd_input.focus()

agent = new Agent("polkitwebkit")
$("#container").appendChild(agent.element)

document.body.addEventListener("keydown", (e) =>
    if e.which == 13
        agent.start_auth()

    else if e.which == 27
        agent.in_auth = false
        DCore.PolkitAgent.cancel_authentication()
)

DCore.signal_connect("auth", (msg) ->
    echo "auth failed"
    echo msg.times
    agent.auth_failed(msg.times)
)
