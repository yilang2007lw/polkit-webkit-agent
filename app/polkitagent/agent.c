/*
 * Copyright (C) 2009 Red Hat, Inc.
 *               2013 Deepin, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Author: David Zeuthen <davidz@redhat.com>
 *         Long Wei <yilang2007lw@gmail.com>
 */

#include <string.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <polkitagent/polkitagent.h>
#include <locale.h>
#include <libintl.h>
#include "listener.h"

#define SM_DBUS_NAME                "org.gnome.SessionManager"
#define SM_DBUS_PATH                "/org/gnome/SessionManager"
#define SM_DBUS_INTERFACE           "org.gnome.SessionManager"
#define SM_CLIENT_DBUS_INTERFACE    "org.gnome.SessionManager.ClientPrivate"

static PolkitAuthority  *authority = NULL;
static PolkitSubject    *session = NULL;
static GDBusConnection  *session_bus_connection = NULL;
static GDBusProxy       *sm_proxy;
static GDBusProxy       *client_proxy = NULL;
static GMainLoop        *loop;

static void
stop_cb (void)
{
    g_main_loop_quit (loop);
}

static gboolean
end_session_response (gboolean is_okay, const gchar *reason)
{
    GVariant *res;
    GError *error = NULL;

    res = g_dbus_proxy_call_sync (client_proxy,
                                  "EndSessionResponse",
                                  g_variant_new ("(bs)",
                                                 is_okay,
                                                 reason),
                                  G_DBUS_CALL_FLAGS_NONE,
                                  -1, /* timeout */
                                  NULL, /* GCancellable */
                                  &error);
    if (! res) {
        g_warning ("Failed to call EndSessionResponse: %s", error->message);
        g_error_free (error);
        return FALSE;
    }
    g_variant_unref (res);

    return TRUE;
}

static void
query_end_session_cb (void)
{
    end_session_response (TRUE, "");
}

static void
end_session_cb (void)
{
    end_session_response (TRUE, "");
    g_main_loop_quit (loop);
}

static void
signal_cb (GDBusProxy *proxy, gchar *sender_name, gchar *signal_name, 
            GVariant *parameters, gpointer user_data)
{
    if (strcmp (signal_name, "Stop") == 0) {
        stop_cb ();

    } else if (strcmp (signal_name, "QueryEndSession") == 0) {
        query_end_session_cb ();

    } else if (strcmp (signal_name, "EndSession") == 0) {
        end_session_cb ();
    }
}

static gboolean
register_client_to_gnome_session (void)
{
    GError     *error = NULL;
    GVariant   *res;
    const char *startup_id;
    const char *app_id;
    char       *client_id;

    startup_id = g_getenv ("DESKTOP_AUTOSTART_ID");
    app_id = "polkit-webkit-authentication-agent-1.desktop";

    sm_proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                              G_DBUS_PROXY_FLAGS_NONE,
                                              NULL, /* GDBusInterfaceInfo */
                                              SM_DBUS_NAME,
                                              SM_DBUS_PATH,
                                              SM_DBUS_INTERFACE,
                                              NULL, /* GCancellable */
                                              &error);
    if (sm_proxy == NULL) {
        g_message("Failed to get session manager: %s", error->message);
        g_error_free (error);
        return FALSE;
    }

    res = g_dbus_proxy_call_sync (sm_proxy,
                                  "RegisterClient",
                                  g_variant_new ("(ss)",
                                                 app_id,
                                                 startup_id),
                                  G_DBUS_CALL_FLAGS_NONE,
                                  -1, /* timeout */
                                  NULL, /* GCancellable */
                                  &error);
    if (! res) {
        g_warning ("Failed to register client: %s", error->message);
        g_error_free (error);
        return FALSE;
    }

    if (! g_variant_is_of_type (res, G_VARIANT_TYPE ("(o)"))) {
        g_warning ("RegisterClient returned unexpected type %s",
                   g_variant_get_type_string (res));
        return FALSE;
    }

    g_variant_get (res, "(&o)", &client_id);

    client_proxy = g_dbus_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                                  G_DBUS_PROXY_FLAGS_NONE,
                                                  NULL, /* GDBusInterfaceInfo */
                                                  SM_DBUS_NAME,
                                                  client_id,
                                                  SM_CLIENT_DBUS_INTERFACE,
                                                  NULL, /* GCancellable */
                                                  &error);
    g_variant_unref (res);
    if (client_proxy == NULL) {
        g_message ("Failed to get client proxy: %s", error->message);
        g_error_free (error);
        return FALSE;
    }

    g_signal_connect (client_proxy, "g-signal", G_CALLBACK (signal_cb), NULL);

    return TRUE;
}

int main (int argc, char **argv)
{
    gint ret;
    PolkitAgentListener *listener;
    GError *error;

    gtk_init (&argc, &argv);

    setlocale (LC_MESSAGES, "");
    textdomain ("polkitagent");

    loop = NULL;
    authority = NULL;
    listener = NULL;
    session = NULL;
    ret = 1;

    loop = g_main_loop_new (NULL, FALSE);

    error = NULL;
    authority = polkit_authority_get_sync (NULL /* GCancellable* */, &error);
    if (authority == NULL) {
        g_warning ("Error getting authority: %s", error->message);
        g_error_free (error);
        goto out;
    }

    listener = polkit_webkit_listener_new ();

    error = NULL;
    session = polkit_unix_session_new_for_process_sync (getpid (), NULL, &error);
    if (error != NULL) {
        g_warning ("Unable to determine the session we are in: %s", error->message);
        g_error_free (error);
        goto out;
    }

    error = NULL;
    if (!polkit_agent_listener_register (listener,
                                         POLKIT_AGENT_REGISTER_FLAGS_NONE,
                                         session,
                                         "/com/deepin/PolicyKit1/AuthenticationAgent",
                                         NULL, /* GCancellable */
                                         &error)) {

        g_warning ("Cannot register authentication agent: %s\n", error->message);
        g_error_free (error);
        goto out;
    }

    session_bus_connection = g_bus_get_sync (G_BUS_TYPE_SESSION, NULL, NULL);

    register_client_to_gnome_session();

    g_main_loop_run (loop);

    ret = 0;

out:
    g_debug ("agent: goto out");

    if (authority != NULL)
        g_object_unref (authority);

    if (session != NULL)
        g_object_unref (session);

    if (listener != NULL)
        g_object_unref (listener);

    if (loop != NULL)
        g_main_loop_unref (loop);

    return ret;
}
