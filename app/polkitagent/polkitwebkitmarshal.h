

#ifndef ___polkit_webkit_marshal_MARSHAL_H__
#define ___polkit_webkit_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:BOOLEAN,BOOLEAN (marshal.list:1) */
extern void _polkit_webkit_marshal_VOID__BOOLEAN_BOOLEAN (GClosure     *closure,
                                                          GValue       *return_value,
                                                          guint         n_param_values,
                                                          const GValue *param_values,
                                                          gpointer      invocation_hint,
                                                          gpointer      marshal_data);

G_END_DECLS

#endif /* ___polkit_webkit_marshal_MARSHAL_H__ */

